package jp.co.FizzBuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FizzBuzzTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void Calculate_01() {
	    assertEquals(FizzBuzz.calculate(2),"2");
	}

	@Test
	public void  Calculate_02() {
		assertEquals(FizzBuzz.calculate(3),"Fizz");
	}

	@Test
	public void  Calculate_03() {
		assertEquals(FizzBuzz.calculate(4),"4");
	}

	@Test
	public void  Calculate_04() {
		assertEquals(FizzBuzz.calculate(5),"Buzz");
	}

	@Test
	public void  Calculate_05() {
		assertEquals(FizzBuzz.calculate(6),"Fizz");
	}

	@Test
	public void  Calculate_06() {
		assertEquals(FizzBuzz.calculate(14),"14");
	}

	@Test
	public void  Calculate_07() {
		assertEquals(FizzBuzz.calculate(15),"FizzBuzz");
	}

	@Test
	public void  Calculate_08() {
		assertEquals(FizzBuzz.calculate(16),"16");
	}
	@Test
	public void Calculate_09() {
	    assertEquals(FizzBuzz.calculate(-2),"-2");
	}

	@Test
	public void  Calculate_10() {
		assertEquals(FizzBuzz.calculate(-3),"Fizz");
	}

	@Test
	public void  Calculate_11() {
		assertEquals(FizzBuzz.calculate(-4),"-4");
	}

	@Test
	public void  Calculate_12() {
		assertEquals(FizzBuzz.calculate(-5),"Buzz");
	}

	@Test
	public void  Calculate_13() {
		assertEquals(FizzBuzz.calculate(-6),"Fizz");
	}

	@Test
	public void  Calculate_14() {
		assertEquals(FizzBuzz.calculate(-14),"-14");
	}

	@Test
	public void  Calculate_15() {
		assertEquals(FizzBuzz.calculate(-15),"FizzBuzz");
	}

	@Test
	public void  Calculate_16() {
		assertEquals(FizzBuzz.calculate(-16),"-16");
	}
	
	@Test
	public void  Calculate_17() {
		assertEquals(FizzBuzz.calculate(0),"0");
	}
}
