package jp.co.FizzBuzz;

public class FizzBuzz {
	public static void main(String[] args) {
		calculate(15);
	}

	static public String calculate(int x) {
		// TODO 自動生成されたメソッド・スタブ
		if(x == 0) {
			System.out.println(x);
			return Integer.toString(x);
		}else if(x % 3 == 0 && x % 5 != 0) {
			System.out.println("Fizz");
			return "Fizz";
		}else if(x % 3 != 0 && x % 5 == 0) {
			System.out.println("Buzz");
			return "Buzz";
		}else if(x % 3 == 0 && x % 5 == 0) {
			System.out.println("FizzBuzz");
			return "FizzBuzz";
		}else {
			System.out.println(x);
			return Integer.toString(x);
		}

	}

}
